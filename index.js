var AWS = require('aws-sdk');
const config = require('./config/config');
var express = require('express'),
    app = express(),
    port = 3000;
app.listen(port);


app.get('/api/sensors', (req, res, next) => {
  AWS.config.update(config.aws_local_config);
  const docClient = new AWS.DynamoDB.DocumentClient();

  const params = {
    TableName: config.aws_table_name
  };

  docClient.scan(params, function(err, data) {
    if (err) {
      res.send({
        success: false,
        message: 'Error: Server error'
      });
    } else {
      res.send({
        success: true,
        message: 'Loaded data',
        data
      });
    }
  });
}); 


app.get('/api/sensors/:id', (req, res, next) => {
  var id = req.params.id;
  AWS.config.update(config.aws_local_config);
  const docClient = new AWS.DynamoDB.DocumentClient();
  var dates = new Date("2018-01-01");
  var startDate = dates.toISOString();
  // var id_wasp = "node_EVENTS";
  var params = {
    TableName : config.aws_table_name,
    KeyConditionExpression: "#sensor = :sensor",
    FilterExpression: "#sensor = :sensor AND #startDate  > :datetime",
    // FilterExpression: "#sensor = :sensor AND #id_wasp = :id_wasp",
    ExpressionAttributeNames:{
        "#sensor": "sensor",
        // "#id_wasp": "id_wasp"
        "#startDate": "datetime"
    },
    ExpressionAttributeValues: {
        ":sensor": id,
        // ":id_wasp": id_wasp
        ":datetime": startDate
    },
    Limit: 10000
  };

  var wholeData = [];

  var resCallback = function(err, data){
    if (err) {
      res.send({
        success: false,
        message: 'Error: Server error'
      });
    } else {
      res.send({
        success: true,
        message: 'Loaded data',
        data
      });
    }
  }

  docClient.scan(params, onScan, resCallback);

  function onScan(err, data, callback){
     if (err) {
      console.log(err);
      callback(err,err);
      // res.send({
      //   success: false,
      //   message: 'Error: Server error'
      // });
    } else {
      wholeData.push(data);
      console.log("Scan succeeded.");
      if (typeof data.LastEvaluatedKey != "undefined") {
        console.log("Scanning for more...");
        params.ExclusiveStartKey = data.LastEvaluatedKey;
        docClient.scan(params, onScan);
      }else{
        callback(null, data);
      }
    }
  }
  
}); 
